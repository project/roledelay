The roledelay module allows you to set a user role that new members are granted after a certain time period from registration. A common use for this is to deny the default 'authenticated user' role permissions to post certain types of content, and create another role which roledelay grants that allows these permissions. This helps to deter all but the most patient trolls, since creating a new account after being blocked becomes a much costlier and less gratifying experience. This can also help encourage new users to read and get a good feel for the type of acceptable posting on your site before being allowed to post themselves. There are two wait periods, which allow for two levels of role granting after the initial 'authenticated user' role.

To install, place the module in your modules directory, and add the mysql tables to your database using the .mysql file. Enable the module in admin/modules, and then set the desired parameters in admin/settings/roledelay.

This module requires cron to function properly.

Send comments to welch@advomatic.com

Originally developed for securingamerica.com by Aaron Welch (crunchywelch) at Advomatic LLC
